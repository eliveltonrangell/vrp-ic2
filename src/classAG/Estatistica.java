package classAG;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author felip
 */
public class Estatistica {
    
    private double fitness,menor,mediaFitness,desvioPadrao;

    
    
    public double getMediaFitness(){
        return mediaFitness;
    }
    
    public void setMediaFitness(double mediaFitness){
        this.mediaFitness = mediaFitness;
    }
    
    public void calcula(Populacao populacao){
        
        media(populacao);
        desvioPadrao(populacao);
    }
    
    /*
    opCruzamento
    OpMutacao
    txCruzamento
    txMutacao
    tamPopulacao
    numGeracoes
    
    
    */
    
    public double media(Populacao lista){
        double mediaFitness = 0, soma=0 , menor = lista.getIndividuos().get(0).getCusto();        
        for (int i = 0; i < AG.TAMANHO_POPULACAO ; i++) {
            soma += lista.getIndividuos().get(i).getCusto();
            if(menor>lista.getIndividuos().get(i).getCusto())
                menor = lista.getIndividuos().get(i).getCusto();
        }
        this.menor = menor;
        this.mediaFitness = soma/AG.TAMANHO_POPULACAO;        
        return truncate(mediaFitness, 5);
        }
    
    public double variancia(Populacao lista){
        double variancia=0;
        double somaQuadradosDesvios=0;
        for (int i = 0; i < AG.TAMANHO_POPULACAO ; i++) {
            double q = lista.getIndividuos().get(i).getCusto();
            double valor = lista.getIndividuos().get(i).getCusto() - this.mediaFitness;
            somaQuadradosDesvios += Math.pow(valor, 2);            
        }       
        variancia = somaQuadradosDesvios/(AG.TAMANHO_POPULACAO-1);
        return truncate(variancia, 5);
    }
    
    public double desvioPadrao(Populacao lista){ 
       double w = variancia(lista);
       this.desvioPadrao = Math.sqrt(variancia(lista));
       return truncate(desvioPadrao,5);
        
    }
    
    public double truncate(Double valor, int precisao) {  
        BigDecimal bd = BigDecimal.valueOf(valor);  
        bd = bd.setScale(precisao, BigDecimal.ROUND_DOWN);  
  
        return bd.doubleValue();  
    }
    
    public String imprimir(int geracao){
          
           return (geracao + " |" + menor  + " |" + desvioPadrao + " |"  + mediaFitness + "%n");
    }  
}
    

