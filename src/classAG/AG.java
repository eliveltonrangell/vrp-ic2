package classAG;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cliente;
import model.Veiculo;

public class AG {

    private Populacao populacao;
    private double chanceMutacao, chanceCruzamento;
    public static int NUMERO_GERACOES, TAMANHO_POPULACAO, TAMANHO_INDIVIDUO;
    public List<Veiculo> veiculo;
    private List<Cliente> clientes;
    public static double[][] DISTANCIAS;
    double totalFitness = 0;
    private Estatistica calcs;

    public AG(int num_geracoes, int tam_populacao, double prob_mut,
            double prob_cruzamento, List<Cliente> clientes, List<Veiculo> veiculo) {
        NUMERO_GERACOES = num_geracoes;
        chanceMutacao = prob_mut;
        chanceCruzamento = prob_cruzamento;
        TAMANHO_POPULACAO = tam_populacao;
        this.veiculo = veiculo;
        this.clientes = clientes;
    }

    public Individuo exec(Selecao selecao, Mutacao mutacao, Cruzamento cruzamento, List<Cliente> clientes, List<Veiculo> veiculo) {
        populacao = new Populacao();
        calcs = new Estatistica();
        TAMANHO_INDIVIDUO = clientes.size() - 1;
        DISTANCIAS = new double[clientes.size()][clientes.size()];
        populacao.gerarPopulacao(clientes, veiculo);

        int cont = 0;
        FileWriter arquivo = null;
        try {
            arquivo = new FileWriter(new File("Exec03.txt"));
        } catch (IOException ex) {
            Logger.getLogger(AG.class.getName()).log(Level.SEVERE, null, ex);
        }
        PrintWriter gravar = new PrintWriter(arquivo);
        gravar.printf("| %2f - %f - %2d - %2d |%n", chanceCruzamento, chanceMutacao, TAMANHO_POPULACAO, NUMERO_GERACOES);
        while (cont <= NUMERO_GERACOES) {
            switch (selecao) {
                case Torneio:
                    populacao = selecaoTorneio(populacao);
                    break;
                case Roleta:
                    populacao = selecaoRoleta(populacao);
                    break;
            }

            switch (cruzamento) {
                case PontoUnico:
                    populacao = cruzamentoUP(populacao);
                    break;
            }

            switch (mutacao) {
                case EM:
                    populacao = mutacaoEM(populacao);
                    break;
            }

            calcs.calcula(populacao);
            calcs.imprimir(cont);
            gravar.printf(calcs.imprimir(cont));
            cont++;

        }
        try {
            arquivo.close();
        } catch (IOException ex) {
            Logger.getLogger(AG.class.getName()).log(Level.SEVERE, null, ex);
        }
        return populacao.getIndividuos().get(0);
    }

    public Populacao cruzamentoUP(Populacao nova_pop) {
        for (int j = 0; j < AG.TAMANHO_POPULACAO - 1; j += 2) {
            double p = Math.random();  //probabilidade
            if (p <= chanceCruzamento) {
                Individuo pai1 = nova_pop.getIndividuos().get(j);
                Individuo pai2 = nova_pop.getIndividuos().get(j + 1);
                List<Integer> solucaoPai1 = pai1.getCromossomo();
                List<Integer> solucaoPai2 = pai2.getCromossomo();
                int x = (int) (Math.random() * (AG.TAMANHO_INDIVIDUO - 1)) + 1;

                Individuo filho1 = new Individuo();
                Individuo filho2 = new Individuo();
                List<Integer> solucaoFilho1 = new ArrayList<Integer>();
                List<Integer> solucaoFilho2 = new ArrayList<Integer>();
                List<Integer> mapeamento1 = new ArrayList<Integer>();
                List<Integer> mapeamento2 = new ArrayList<Integer>();
                mapeamento1.addAll(solucaoPai1.subList(0, x));
                mapeamento2.addAll(solucaoPai2.subList(0, x));

                for (int i = 0; i < TAMANHO_INDIVIDUO; i++) {
                    if (i < x) {
                        solucaoFilho1.add(solucaoPai2.get(i));  //
                        solucaoFilho2.add(solucaoPai1.get(i));  //
                        continue;
                    }

                    if (mapeamento2.contains(solucaoPai1.get(i))) {  //Verifica conflito de genes
                        int index = mapeamento2.indexOf(solucaoPai1.get(i));
                        while (mapeamento2.contains(solucaoPai1.get(index))) {
                            index = mapeamento2.indexOf(solucaoPai1.get(index));
                        }
                        solucaoFilho1.add(solucaoPai1.get(index));
                    } else {
                        solucaoFilho1.add(solucaoPai1.get(i));
                    }

                    if (mapeamento1.contains(solucaoPai2.get(i))) {
                        int index = mapeamento1.indexOf(solucaoPai2.get(i));
                        while (mapeamento1.contains(solucaoPai2.get(index))) {
                            index = mapeamento1.indexOf(solucaoPai2.get(index));
                        }
                        solucaoFilho2.add(solucaoPai2.get(index));
                    } else {
                        solucaoFilho2.add(solucaoPai2.get(i));
                    }
                }

                filho1.setCromossomo(solucaoFilho1);
                filho2.setCromossomo(solucaoFilho2);
                filho1.avaliarFitness(clientes, veiculo);
                filho2.avaliarFitness(clientes, veiculo);
                if (filho1.getFitness() > pai1.getFitness()) {
                    int index = nova_pop.getIndividuos().indexOf(pai1);
                    nova_pop.getIndividuos().set(index, filho1);

                }
                if (filho2.getFitness() > pai2.getFitness()) {
                    int index = nova_pop.getIndividuos().indexOf(pai2);
                    nova_pop.getIndividuos().set(index, filho2);

                }

            }

        }

        Collections.sort(nova_pop.getIndividuos());
        return nova_pop;
    }

    public Populacao mutacaoEM(Populacao pop) {
        for (int j = 0; j < AG.TAMANHO_POPULACAO - 1; j++) {
            double p = Math.random();
            if (p <= chanceMutacao) {
                Individuo pai = pop.getIndividuos().get(j);
                List<Integer> solucaoPai = pai.getCromossomo();
                int x1 = (int) (Math.random() * (AG.TAMANHO_INDIVIDUO));
                int x2 = (int) (Math.random() * (AG.TAMANHO_INDIVIDUO));
                if (x2 < x1) {
                    int x = x1;
                    x1 = x2;
                    x2 = x;
                }
                Individuo filho = new Individuo();
                List<Integer> solucaoFilho = new ArrayList<Integer>();
                for (int i = 0; i < TAMANHO_INDIVIDUO; i++) {
                    if (i == x1) {
                        solucaoFilho.add(solucaoPai.get(x2));
                        continue;
                    }
                    if (i == x2) {
                        solucaoFilho.add(solucaoPai.get(x1));
                        continue;
                    }
                    solucaoFilho.add(solucaoPai.get(i));
                }

                filho.setCromossomo(solucaoFilho);
                filho.avaliarFitness(clientes, veiculo);

                if (filho.getFitness() > pai.getFitness()) {
                    int index = pop.getIndividuos().indexOf(pai);
                    pop.getIndividuos().set(index, filho);

                }
            }
        }
        Collections.sort(pop.getIndividuos());
        return pop;
    }

    public Populacao selecaoRoleta(Populacao pop) {
        List<Individuo> selecionados = new ArrayList<Individuo>();
        for (int j = 0; j < TAMANHO_POPULACAO; j++) {

            int soma = 0;
            for (Individuo i : pop.getIndividuos()) {
                soma += i.getCusto();
            }
            //double r = Math.random();
            double somaP = 0;
            for (int i = 0; i < TAMANHO_POPULACAO; i++) {
                double x = pop.getIndividuos().get(i).getCusto() / soma;
                somaP += x;
                if (x < somaP) {
                    selecionados.add(pop.getIndividuos().get(i));
                    break;
                }
            }
        }
        return pop;
    }

    public Populacao selecaoTorneio(Populacao pop) {
        List<Individuo> torneio = new ArrayList<Individuo>();
        Populacao novaPopulacao = new Populacao();
        System.out.println(populacao);

        Collections.sort(pop.getIndividuos());
        for (int i = 0; i < TAMANHO_POPULACAO; i++) {
            for (int j = 0; j < 10; j++) {
                int x = (int) (Math.random() * pop.getIndividuos().size());
                torneio.add(pop.getIndividuos().get(x));
            }
            Collections.sort(torneio);
            novaPopulacao.getIndividuos().add(torneio.get(0));
        }

        return novaPopulacao;
    }

    /**
     * @return the populacao
     */
    public Populacao getPopulacao() {
        return populacao;
    }

    /**
     * @param populacao the populacao to set
     */
    public void setPopulacao(Populacao populacao) {
        this.populacao = populacao;
    }

}
