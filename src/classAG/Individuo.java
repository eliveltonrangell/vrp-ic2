package classAG;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import model.Cliente;
import model.Veiculo;

public class Individuo implements Comparable<Individuo> {

    private List<Integer> cromossomo = new ArrayList<Integer>();
    private List<Integer> indiceLimRota = new ArrayList<Integer>();
    private double fitness;
    private double custo;

    @Override
    public int compareTo(Individuo i) {

        return (int) (custo - i.custo);
    }

    public List<Integer> getCromossomo() {
        return cromossomo;
    }

    public void setCromossomo(List<Integer> solucao) {

        this.cromossomo = solucao;

    }

    public List<Integer> getIndiceLimRota() {
        return indiceLimRota;
    }

    public void setIndiceLimRota(List<Integer> solucao) {

        this.indiceLimRota = solucao;

    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    @Override
    public String toString() {
        return cromossomo.toString() + " Fitness: " + fitness + "  Custo" + custo;
    }

    public double getCusto() {
        return custo;
    }

    public void setCusto(double soma) {
        this.custo = soma;
    }

    public void gerarAleatorio(List<Cliente> clientes) {
        cromossomo = new ArrayList<Integer>();
        List<Integer> temp = new ArrayList<Integer>();
        for (int i = 0; i < clientes.size(); i++) {
            temp.add(clientes.get(i).getId());
            for (int j = 0; j < clientes.size(); j++) {
                Point p1 = clientes.get(i).getCoordenada();
                Point p2 = clientes.get(j).getCoordenada();
                if (i == j) {
                    continue;
                }
                double x = Math.sqrt(Math.pow((p1.x - p2.x), 2) + Math.pow((p1.y - p2.y), 2));
                AG.DISTANCIAS[i][j] = x;
            }
        }

        for (int i = 0; i < AG.TAMANHO_INDIVIDUO; i++) {
            if (clientes.size() == 0) {
                break;
            }
            int r = (int) (Math.random() * temp.size() - 1) + 1;
            cromossomo.add(temp.get(r));
            temp.remove(r);
        }
    }

    public void avaliarCusto(List<Cliente> clientes, List<Veiculo> veiculo) {

        //int qtdVeiculo = 1;
        List<Integer> tempIndices = new ArrayList<Integer>();

        double somaCarga = 0;
        double somaDistancia = 0;
        double somaCargaClientes = 0;

        int iCliente = 1;
        int iVeiculo = 0;

        int totalVeiculo = veiculo.size() - 1;
        int totalCliente = clientes.size() - 1;

        boolean troca = true;

        for (int i = 1; i <= AG.TAMANHO_INDIVIDUO; i++) {
            somaCargaClientes += clientes.get(i).getDemanda();
        }

        if (veiculo.get(0).getCapacidade() >= somaCargaClientes) {
            int aux = 0 + (int) (Math.random() * ((AG.TAMANHO_INDIVIDUO - 1) - 0) + 1);
            while ((tempIndices.size() < veiculo.size() - 1) && (aux != AG.TAMANHO_INDIVIDUO - 1)) {
                tempIndices.add(aux);

                aux = (aux + 1) + (int) (Math.random() * ((AG.TAMANHO_INDIVIDUO - 2) - (aux + 1)) + 1);
            }
            tempIndices.add(aux);
        }

        //troca = setTroca(veiculo, clientes);
        boolean temVeiculo = true;
        // int i = 1;
        do {
            if (temVeiculo) {
                if ((somaCarga + clientes.get(iCliente).getDemanda() <= veiculo.get(iVeiculo).getCapacidade()) && troca) {
                    if (somaCarga == 0) {
                        somaDistancia += AG.DISTANCIAS[0][cromossomo.get(iCliente - 1)];
                    } else {
                        somaDistancia += AG.DISTANCIAS[cromossomo.get(iCliente - 2)][cromossomo.get(iCliente - 1)];
                    }
                    somaCarga += clientes.get(iCliente).getDemanda();
                } else {
                    somaDistancia += AG.DISTANCIAS[cromossomo.get(iCliente - 1)][0];

                    if (iVeiculo <= totalVeiculo) {
                        somaDistancia += AG.DISTANCIAS[0][cromossomo.get(iCliente - 1)];
                        somaCarga = clientes.get(iCliente).getDemanda();
                        indiceLimRota.add(iCliente - 2);
                        iVeiculo += 1; //N�o precisa dessa vari�vel
                    } else {  // N�o tem mais ve�culos
                        temVeiculo = false;
                        break;
                    }
                }
            }
            iCliente += 1;
            if (iCliente > totalCliente) {
                somaDistancia += AG.DISTANCIAS[cromossomo.get(iCliente - 2)][0];
                indiceLimRota.add(iCliente - 2);
            }
        } while (iCliente <= totalCliente);

        setCusto(somaDistancia);
    }

    public void avaliarFitness(List<Cliente> clientes, List<Veiculo> veiculo) {
        avaliarCusto(clientes, veiculo);
        //fitness = -1 * custo + Populacao.PIOR_CUSTO;
        fitness = -1 * custo + Populacao.PIOR_CUSTO;

    }

}
