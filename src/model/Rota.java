/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author elive
 */
public class Rota {

    private int idVeiculo;
    private ArrayList<Point> coordenadas;
    private double somaEuclidiana;

    public double getSomaEuclidiana() {
        return somaEuclidiana;
    }

    public void setSomaEuclidiana(double somaEuclidiana) {
        this.somaEuclidiana = somaEuclidiana;
    }
    

    public int getIdVeiculo() {
        return idVeiculo;
    }

    public void setIdVeiculo(int idVeiculo) {
        this.idVeiculo = idVeiculo;
    }

    public ArrayList<Point> getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(ArrayList<Point> coordenadas) {
        this.coordenadas = coordenadas;
    }

    public void GerarRotaAleatoria(ArrayList<Cliente> c, ArrayList<Veiculo> v, ArrayList<Rota> r) {

        ArrayList<Cliente> nc = (ArrayList) c.clone();
        int capacidadeV;
        int capacidadeC;
        //Collections.shuffle(nc);

        for (Veiculo nv : v) {

            Rota rota = new Rota();
            ArrayList<Point> tmpP = new ArrayList();
            //tmpP.add(nc.get(0).getCoordenada());
            //nc.remove(0);
            rota.setIdVeiculo(nv.getId());
            capacidadeV = 0;
            int capacidadeVtotal = nv.getCapacidade();
            boolean preencheu = false;
            boolean cond = false;
            int i = 0;

            if (nc.isEmpty()) {
                break;
            }

            while ((preencheu != true)) {
                if (!nc.isEmpty()) {
                    capacidadeC = nc.get(0).getDemanda();
                    if (capacidadeVtotal >= (capacidadeV + capacidadeC)) {
                        tmpP.add(nc.get(0).getCoordenada());
                        nc.remove(0);
                        capacidadeV += capacidadeC;
                    } else {
                        while (cond != true) {
                            capacidadeC = nc.get(i).getDemanda();
                            if (capacidadeVtotal >= (capacidadeV + capacidadeC)) {
                                tmpP.add(nc.get(i).getCoordenada());
                                nc.remove(i);
                                capacidadeV += capacidadeC;
                                cond = true;
                            } else if (i == nc.size() - 1) {
                                cond = true;
                            } else {
                                cond = false;
                            }
                            i++;
                        }
                        preencheu = true;
                    }
                } else {
                    preencheu = true;
                }
            }
            rota.setCoordenadas(tmpP);
            r.add(rota);
        }

        if (!nc.isEmpty()) {
            //GerarRotaAleatoria(nc, v, d, r);
        }

    }

}
