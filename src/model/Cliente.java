/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Point;

/**
 *
 * @author elive
 */
public class Cliente {

    private int id;
    private Point coordenada;

    public void setId(int id) {
        this.id = id;
    }

    public void setCoordenada(Point coordenada) {
        this.coordenada = coordenada;
    }

    public void setDemanda(int demanda) {
        this.demanda = demanda;
    }
    private int demanda;

//    public Cliente(int id, Point coordenadas) {
//        this.id = id;
//        this.coordenadas = coordenadas;
//    }
    
    public Cliente(int id, Point coordenada, int demanda){
        this.id = id;
        this.coordenada = coordenada;
        this.demanda = demanda;
    }

    public int getId() {
        return id;
    }

    public Point getCoordenada() {
        return coordenada;
    }

    public int getDemanda() {
        return demanda;
    }


    @Override
    public String toString() {
        return id + " " + coordenada.x + " " + coordenada.y + " " + demanda;
    }

}
