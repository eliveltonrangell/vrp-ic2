/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author elive
 */
public class Veiculo {
    
    private int id;
    private int capacidade;
    
    public Veiculo(int id, int capacidade){
        this.id = id;
        this.capacidade = capacidade;
    }

    public int getId() {
        return id;
    }


    public int getCapacidade() {
        return capacidade;
    }    
}
