package testes;


import classes.Functions;
import java.awt.Point;
import java.util.ArrayList;
import model.Cliente;
import model.Rota;
import model.Veiculo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author elive
 */
public class teste {

    public static void main(String[] args) {
        Rota rota = new Rota();

        ArrayList<Rota> arrR = new ArrayList<>();

        ArrayList<Cliente> arrC = new ArrayList<>();

        ArrayList<Veiculo> arrV = new ArrayList<>();

        Cliente c0 = new Cliente(0, (new Point(0, 0)), 25);
        arrC.add(c0);

        Cliente c1 = new Cliente(1, (new Point(1, 1)), 10);
        arrC.add(c1);

        Cliente c2 = new Cliente(2, (new Point(2, 2)), 25);
        arrC.add(c2);
        
        Cliente c3 = new Cliente(2, (new Point(3, 3)), 10);
        arrC.add(c3);
        
        Cliente c4 = new Cliente(2, (new Point(4, 4)), 15);
        arrC.add(c4);
        
        Cliente c5 = new Cliente(2, (new Point(5, 5)), 10);
        arrC.add(c5);


        //Veiculos
        Veiculo v = new Veiculo(0, 500);
        arrV.add(v);

        Veiculo v1 = new Veiculo(1, 40);
        arrV.add(v1);

        rota.GerarRotaAleatoria(arrC, arrV, arrR);
        
        for(Rota r1 : arrR){
            System.out.println("Rota "+r1.getIdVeiculo());
            for(Point p: r1.getCoordenadas()){
                System.out.println("Coordenadas ["+p.getX()+", "+p.getY()+"]");
            }
        }

    }
}
