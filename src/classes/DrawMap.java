/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import model.Cliente;
import model.Decisao;
import model.Rota;

/**
 *
 * @author elive
 */
public class DrawMap {

    private Color corPonto = Color.blue;
    private Color corQuadrado = Color.red;
    private ArrayList<Color> colors = new ArrayList<>();

    public Color getColor() {
        Functions f = new Functions(0, 255);
        int r = f.gerarNumeroAleatorio();
        int g = f.gerarNumeroAleatorio();
        int b = f.gerarNumeroAleatorio();
        return new Color(r, g, b);

    }

    public void drawPointOnMap(JPanel panel, Point p, ArrayList<Cliente> lstCli, Cliente cli, Decisao d, Integer countDeposit) {
        Graphics g = panel.getGraphics();
        //g.setColor(new Color(60, 63, 65));
        //g.fillRect(2, 2, 496, 496);

        if (d == d.PrintAll) {
            g.setColor(corQuadrado);
            g.fillRect(lstCli.get(0).getCoordenada().x - 5, lstCli.get(0).getCoordenada().y - 5, 10, 10);

            for (int i = 1; i < lstCli.size(); i++) {
                Point pnt = lstCli.get(i).getCoordenada();
                g.setColor(corPonto);
                g.fillOval(pnt.x - 3, pnt.y - 3, 6, 6);
                g.drawString("" + i, pnt.x - 4, pnt.y - 4);
                //g.finalize();
            }

        } else if (d == d.PrintSimple) {
            if (cli == null && countDeposit == 0) {
                g.setColor(corQuadrado);
                g.fillRect(p.x - 5, p.y - 5, 10, 10);
                g.drawString("", p.x, p.y);
                //g.finalize();
            } else {
                g.setColor(corPonto);
                g.fillOval(p.x - 3, p.y - 3, 6, 6);
                g.drawString("" + cli.getId(), p.x - 4, p.y - 4);
                //g.finalize();
            }
        }
    }
    
    public void drawLineOnMap(JPanel panel, ArrayList<ArrayList> listas) {
        Graphics g = panel.getGraphics();
        g.setColor(new Color(60, 63, 65));
        g.fillRect(2, 2, 496, 496);

        ArrayList<Rota> rota = listas.get(0);
        ArrayList<Cliente> lstClient = listas.get(1);
        ArrayList<JLabel> labels = listas.get(2);

        Functions f = new Functions(0, 0);
        int x1, x2, y1, y2 = 0;

        double somaEuclidiana = 0.;
        double totalSomaEuclidiana = 0.;
        int somaCidades = 0;

        for (int i = 0; i < rota.size(); i++) {
            g.setColor(getColor());
            double somaRota = 0;
            for (int j = 0; j < rota.get(i).getCoordenadas().size(); j++) {
                x1 = rota.get(i).getCoordenadas().get(j).x;
                y1 = rota.get(i).getCoordenadas().get(j).y;

                if (j == rota.get(i).getCoordenadas().size() - 1) {
                    x2 = rota.get(i).getCoordenadas().get(0).x;
                    y2 = rota.get(i).getCoordenadas().get(0).y;
                } else {
                    x2 = rota.get(i).getCoordenadas().get(j + 1).x;
                    y2 = rota.get(i).getCoordenadas().get(j + 1).y;
                }
                somaEuclidiana = f.CalculoRetaEuclidiana(x1, x2, y1, y2);
                totalSomaEuclidiana += somaEuclidiana;
                somaRota += somaEuclidiana;
                g.drawString(String.format("%.1f", somaEuclidiana), (x1 + x2) / 2, (y1 + y2) / 2);
                g.drawLine(x1, y1, x2, y2);
                somaCidades++;
                rota.get(i).setSomaEuclidiana(somaRota);

            }
        }
        drawPointOnMap(panel, null, lstClient, null, Decisao.PrintAll, 0);
        labels.get(0).setText("Posição: ");
        labels.get(1).setText("Custo Total: " + (String.format("%.2f", totalSomaEuclidiana)));
        labels.get(2).setText("Veículos Utilizados: " + rota.size());
        labels.get(3).setText("Cidades Visitadas: " + (somaCidades - rota.size()));

    }

    public void drawPointOnMap2(JPanel panel, ArrayList<Cliente> lstCli) {
        Graphics g = panel.getGraphics();
        g.setColor(new Color(60, 63, 65));
        g.fillRect(2, 2, 496, 496);
        g.setColor(Color.BLACK);
        for (int i = 0; i < lstCli.size(); i++) {
            Point p = lstCli.get(i).getCoordenada();
            //g.fillOval(p.x * 5 - 1, p.y * 5 - 1, 5, 5);
            g.drawString("" + i, p.x - 4, p.y - 4);
            g.setColor(Color.BLUE);
        }
    }

    public void DrawLine2(ArrayList<ArrayList<Integer>> rotas, ArrayList<Cliente> clientes, JPanel panel) {
        Graphics g = panel.getGraphics();
        g.setColor(new Color(60, 63, 65));
        g.fillRect(2, 2, 496, 496);

        Cliente dep = clientes.get(rotas.get(0).get(0));
        Point depositoPonto = dep.getCoordenada();

        for (int i = 0; i < rotas.size(); i++) {
            List<Integer> rota = rotas.get(i);
            g.setColor(getColor());
            for (int j = 0; j < rota.size() - 1; j++) {
                Cliente c = clientes.get(rota.get(j));
                Point ponto = c.getCoordenada();
                Cliente c1 = clientes.get(rota.get(j + 1));
                Point ponto1 = c1.getCoordenada();

                Object dist = Math.sqrt(Math.pow(ponto1.x - ponto.x, 2) + Math.pow(ponto1.y - ponto.y, 2));
                String resultado = String.format("%.1f", dist);

                g.drawString(resultado, ((ponto.x + ponto1.x) / 2), ((ponto.y + ponto1.y) / 2));

                g.fillOval(ponto.x - 1, ponto.y - 1, 5, 5);
                g.drawString(new Integer(rota.get(j)).toString(), ponto.x - 5, ponto.y - 5);
                g.drawLine(ponto.x, ponto.y, ponto1.x, ponto1.y);

            }

        }
        g.setColor(Color.BLACK);
        g.fillOval(depositoPonto.x - 2, depositoPonto.y - 2, 8, 8);
        //repaint();
    }

}
