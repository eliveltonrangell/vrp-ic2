/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import model.Cliente;
import model.Veiculo;

/**
 *
 * @author elive
 */
public class ImportExportFlie {

    //Variaveis
    String localFile;

    public void ImportFile(ArrayList<Cliente> lstClient, ArrayList<Veiculo> lstVeiculo, JPanel panel) {
        JFileChooser file = new JFileChooser();
        file.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int i = file.showSaveDialog(null);
        if (i == 1) {
            localFile = "";
        } else {
            FileReader readerFile;
            try {
                readerFile = new FileReader(file.getSelectedFile());

                BufferedReader br = new BufferedReader(readerFile);
                String line = "";
                int nVeiculos = 0;
                int nClientes = 0;

                while ((line = br.readLine()) != null) {
                    String[] lines = line.split(" ");
                    int totVeiculo = Integer.parseInt(lines[0]);
                    while (nVeiculos < totVeiculo) {
                        line = br.readLine();
                        lines = line.split(" ");
                        Veiculo v = new Veiculo(nVeiculos, Integer.parseInt(lines[0]));
                        lstVeiculo.add(v);
                        nVeiculos++;
                    }

                    line = br.readLine();
                    lines = line.split(" ");
                    int totClientes = Integer.parseInt(lines[0]);

                    while (nClientes < totClientes) {
                        line = br.readLine();
                        lines = line.split(" ");
                        if (lines.length == 2) {
                            //lines = line.split(" ");
                            //deposito.setId(0);
                            //deposito.setCoordenada(new Point(Integer.parseInt(lines[0]), Integer.parseInt(lines[1])));
                            Cliente c1 = new Cliente(nClientes, new Point(Integer.parseInt(lines[0]), Integer.parseInt(lines[1])), 0);
                            line = br.readLine();
                            lines = line.split(" ");
                        }
                        Cliente c = new Cliente(nClientes, (new Point(Integer.parseInt(lines[0]), Integer.parseInt(lines[1]))), Integer.parseInt(lines[2]));
                        nClientes++;
                        lstClient.add(c);
                    }
                }
                JOptionPane.showMessageDialog(null, "Importação realizada com sucesso!");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void ImportFile2(ArrayList<Cliente> lstClient, ArrayList<Veiculo> lstVeiculo, JPanel panel) {
        JFileChooser file = new JFileChooser();
        file.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int i = file.showSaveDialog(null);
        if (i == 1) {
            localFile = "";
        } else {
            FileReader readerFile;
            try {
                readerFile = new FileReader(file.getSelectedFile());

                BufferedReader br = new BufferedReader(readerFile);
                String line = "";
                int nVeiculos = 0;
                int nClientes = 0;

                while ((line = br.readLine()) != null) {
                    String[] lines = line.split(" ");
                    int totVeiculo = Integer.parseInt(lines[0]);
                    while (nVeiculos < totVeiculo) {
                        line = br.readLine();
                        lines = line.split(" ");
                        Veiculo v = new Veiculo(nVeiculos, Integer.parseInt(lines[0]));
                        lstVeiculo.add(v);
                        nVeiculos++;
                    }

                    line = br.readLine();
                    lines = line.split(" ");
                    int totClientes = Integer.parseInt(lines[0]);

                    while (nClientes <= totClientes) {
                    line = br.readLine();
                    lines = line.split(" ");

                    Point ponto = new Point(
                            (int) (Double.parseDouble(lines[0])),
                            (int) Double.parseDouble(lines[1]));
                    int quantidade;
                    if (nClientes != 0) {
                        quantidade = Integer.parseInt(lines[2]);
                    } else {
                        quantidade = 0;
                    }
                    Cliente c = new Cliente(nClientes, ponto, quantidade);
                    lstClient.add(c);
                    //lstClient.get(nClientes).setDemanda(quantidade);
                    nClientes++;

                }
                }
                JOptionPane.showMessageDialog(null, "Importação realizada com sucesso!");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void exportFile(ArrayList<Cliente> lstClient, ArrayList<Veiculo> lstVeiculo) {
        if (!lstClient.isEmpty() && !lstVeiculo.isEmpty()) {
            JFileChooser arquivo = new JFileChooser();
            arquivo.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int resultadoArq = arquivo.showSaveDialog(null);
            if (resultadoArq == JFileChooser.CANCEL_OPTION) {
                return;
            }
            FileWriter file2;
            try {
                file2 = new FileWriter(arquivo.getSelectedFile());

                if (file2 == null) {
                    JOptionPane.showMessageDialog(null, "Nome de Arquivo Inválido", "Nome de Arquivo Inválido", javax.swing.JOptionPane.ERROR_MESSAGE);
                } else {

                    PrintWriter pw = new PrintWriter(file2);
                    pw.println(lstVeiculo.size());
                    for (int i = 0; i < lstVeiculo.size(); i++) {
                        pw.println(lstVeiculo.get(i).getCapacidade());
                    }
                    pw.println(lstClient.size());
                    //pw.println(deposito.getCoordenada().x + " " + deposito.getCoordenada().y);
                    pw.println(lstClient.get(0).getCoordenada().x + " " + lstClient.get(0).getCoordenada().y);
                    for (int i = 1; i < lstClient.size(); i++) {
                        pw.println(lstClient.get(i).getCoordenada().x + " " + lstClient.get(i).getCoordenada().y + " " + lstClient.get(i).getDemanda());
                    }
                    file2.close();
                }
                JOptionPane.showMessageDialog(null, "Exportação realizada com sucesso!");

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(null, "O mapa está vazio, verifique...", "O mapa está vazio, verifique...", javax.swing.JOptionPane.ERROR_MESSAGE);
        }
    }

}
