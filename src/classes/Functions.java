/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.ArrayList;
import java.util.Random;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

/**
 *
 * @author elive
 */
public class Functions {

    private int minimo;
    private int maximo;

    public Functions(int minimo, int maximo) {
        this.minimo = minimo;
        this.maximo = maximo;
    }

    public int gerarDemandaAleatoria(JCheckBox chk, int num) {

        if (chk.isSelected()) {
            Random random = new Random();
        return random.nextInt((num - 0) + 1) + 0;
        } else {
            return num;
        }
    }

    public int gerarNumeroAleatorio() {
        Random random = new Random();
        return random.nextInt((maximo - minimo) + 1) + minimo;
    }
    
    public Double CalculoRetaEuclidiana(int x1, int x2, int y1, int y2){
        return Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y1 - y2), 2));
    }
    
    public void PreencherCombo(JComboBox cbox, String[] str){
        for(String a : str){
            cbox.addItem(a);
        }
    }

}
